var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT a.first_name, a.last_name, r.resume_name FROM account a JOIN resume r on r.account_id = a.account_id ' +
                'ORDER BY a.first_name';
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getUser = function(account_id, callback) {
    var query = 'CALL user_getinfo(?)';
    var queryData = [account_id]
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.save = function(params, callback) {
    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume (resume_name, account_id) VALUES (?)';
    var queryData = [params.resume_name, params.account_id];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.save_school = function(school_id, resume_id, callback) {
    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?)';
    var queryData = [resume_id, school_id];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.save_company = function(company_id, resume_id, callback) {
    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?)';
    var queryData = [resume_id, company_id];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.save_skill = function(skill_id, resume_id, callback) {
    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES (?)';
    var queryData = [resume_id, skill_id];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? where resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};
