var express = require('express');
var router = express.Router();
var company_dal = require('../model/company_dal');
var address_dal = require('../model/address_dal');
var account_dal = require('../model/account_dal');
var resume_dal = require('../model/resume_dal');

// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });
});


// Return the select user form
router.get('/add/selectuser', function(req, res){
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/userSelect', {'account': result});
        }
    });
});


router.get('/add', function(req, res){
    resume_dal.getUser(req.query.account_id, function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {account: result[0][0], school: result[1], company: result[2], skill: result[3]});
        }
    });
});


// Insert a new resume
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.resume_name == "") {
        res.send('Please provide a resume name.');
    }
    else if(req.body.company_id == null) {
        res.send('Please select a company.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.save(req.body, function(err,result) {
            var resume_id = result.insertId;
            resume_dal.save_school(req.body.school_id, resume_id, function(err, result) {
                resume_dal.save_company(req.body.company_id, resume_id, function(err, result){
                   resume_dal.save_skill(req.body.skill_id, resume_id, function(err, result) {
                       if (err) {
                           console.log(err)
                           res.send(err);
                       }
                       else {
                           resume_dal.edit(resume_id, function(err, result){
                               res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3], was_successful: true});
                           });
                       }
                   });
                });
            });
        });
    }
});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

module.exports = router;
